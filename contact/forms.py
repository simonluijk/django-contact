from django import forms
from django.utils.translation import ugettext_lazy as _


class ContactForm(forms.Form):
    your_name = forms.CharField(max_length=50, label=_(u'Your name'))
    your_email = forms.EmailField(max_length=50, label=_(u'Your email'))
    message = forms.CharField(widget=forms.Textarea(), label=_(u'Message'))


## NOTE: Not used now, and needs fixing!
#from django.conf import settings
#from django.utils.encoding import smart_str
#from django.contrib.sites.models import Site
#from akismet import Akismet, AkismetError
#    def clean(self):
#        if self._errors:
#            return None
#        if hasattr(settings, 'AKISMET_API_KEY'):
#            url = 'http://%s/' % Site.objects.get_current().domain
#            akismet = Akismet(key=settings.AKISMET_API_KEY, blog_url=url)
#            try:
#                if akismet.verify_key():
#                    result = akismet.comment_check(smart_str(self.cleaned_data['message']), data={
#                        'comment_type': 'comment',
#                        'comment_author': self.cleaned_data['your_name'],
#                        'comment_author_email': self.cleaned_data['your_email'],
#                        'referer': self.request.META.get('HTTP_REFERER', ''),
#                        'user_ip': self.request.META.get('REMOTE_ADDR', ''),
#                        'user_agent': self.request.META.get('HTTP_USER_AGENT', ''),
#                    })
#                    if result:
#                        raise forms.ValidationError(u'Akismet thinks this message is spam')
#            except AkismetError, e:
#                pass
#            return super(ContactForm, self).clean()
