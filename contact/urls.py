try:
    from django.conf.urls import patterns, url
except ImportError:
    from django.conf.urls.defaults import patterns, url
from django.views.generic import TemplateView

from .views import ContactView


urlpatterns = patterns('',
    url(r'^$', ContactView.as_view(), name='contact-form'),
    url(r'^sent/$', TemplateView.as_view(template_name='contact/sent.html'),
        name='contact-sent'),
)
