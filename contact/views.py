from hashlib import sha1

from django.core.urlresolvers import reverse
from django.core.mail import EmailMessage
from django.conf import settings
from django.views.generic import View, FormView
from django.template import RequestContext
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

from .forms import ContactForm


try:
    TO = settings.SUPPORT_STAFF
except AttributeError:
    TO = settings.MANAGERS
TO = ['"%s" <%s>' % (email[0], email[1]) for email in TO]


class ContactView(FormView, View):
    template_name = 'contact/form.html'
    form_class = ContactForm

    def get_success_url(self):
        return reverse('contact-sent')

    def form_valid(self, form):
        current_site = Site.objects.get_current()
        contact_key = sha1(form.cleaned_data['your_email']).hexdigest()[:10]
        ctx = RequestContext(self.request, dict(form.cleaned_data,
                                                site=current_site,
                                                contact_key=contact_key))
        subject = render_to_string('contact/message_subject.txt', ctx).strip()
        message = render_to_string('contact/message.txt', ctx)
        from_name = form.cleaned_data['your_name']
        from_email = form.cleaned_data['your_email']

        email = EmailMessage(subject, message, to=TO, headers={
            'Reply-To': '"%s" <%s>' % (from_name, from_email)})
        email.send()

        return super(ContactView, self).form_valid(form)
